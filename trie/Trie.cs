﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trie
{
    class Trie
    {
        private Node root;

        public Trie()
        {
            root = new Node(0);
        }

        public void Add(string word)
        {
            root.Add(word);
        }

        public bool Delete(string word)
        {
            return root.Delete(word);
        }

        public void MakeTree(TreeView tw)
        {
            tw.Nodes.Clear();
            TreeNode node = new TreeNode("");
            foreach (KeyValuePair<char, Node> p in root.subNodes)
            {
                TreeNode n = new TreeNode(p.Key.ToString());
                p.Value.MakeSubTree(n);
                tw.Nodes.Add(n);
            }
            tw.ExpandAll();
        }

        public int Count(string word)
        {
            return root.Count(word, "");
        }
    }
}
